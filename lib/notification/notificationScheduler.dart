import 'package:flutter/material.dart';
import 'package:pidn_mobile/notification/notificationService.dart';
import 'package:pidn_mobile/provider/configProvider.dart';

class NotificationScheduler {
  static final NotificationScheduler _scheduler = NotificationScheduler._internal();

  factory NotificationScheduler() {
    return _scheduler;
  }

  NotificationScheduler._internal();

  /// reschedules all notifications
  // TODO: Add notifications for next few days + weighting
  Future schedule() async {
    await NotificationService().init();
    await NotificationService().cancelAll();

    TimeOfDay spamStartTime = await ConfigProvider().getSpamStartTime();
    DateTime now = DateTime.now();
    DateTime spamStartDateTime = DateTime(now.year, now.month, now.day, spamStartTime.hour, spamStartTime.minute);
    DateTime endDateTime = spamStartDateTime.add(const Duration(days: 1));
    endDateTime = DateTime(endDateTime.year, endDateTime.month, endDateTime.day, 0, 0); // start of next day
    DateTime nextScheduleDateTime = spamStartDateTime;
    while (nextScheduleDateTime.isBefore(endDateTime)) {
      if (nextScheduleDateTime.isBefore(now)) {
        nextScheduleDateTime = nextScheduleDateTime.add(const Duration(minutes: 30));
        continue;
      }
      DateTime addedDateTime = await NotificationService().scheduleNotification(nextScheduleDateTime);
      nextScheduleDateTime = addedDateTime.add(const Duration(minutes: 30));
    }
  }
}