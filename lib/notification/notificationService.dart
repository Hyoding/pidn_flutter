import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/data/latest_all.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class NotificationService {
  static final NotificationService _notificationService = NotificationService._internal();

  final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  factory NotificationService() {
    return _notificationService;
  }

  NotificationService._internal();

  Future<void> init() async {
    final AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
    final IOSInitializationSettings initializationSettingsIOS = IOSInitializationSettings();
    final InitializationSettings initializationSettings =
    InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: null);

    tz.initializeTimeZones();

    await _flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: _selectNotification);
  }


  Future _selectNotification(String? payload) async {
    print(payload);
    //Handle notification tapped logic here
  }

  /// just use for testing
  Future sendNotification() async {
    NotificationDetails platformChannelSpecifics = _getiOSNotificationDetails();
    await _flutterLocalNotificationsPlugin.show(
        1,
        "DON'T EAT NO MORE",
        "It's past 7PM",
        platformChannelSpecifics,
        payload: 'someData',
    );
  }

  NotificationDetails _getiOSNotificationDetails() {
    const IOSNotificationDetails iOSPlatformChannelSpecifics = IOSNotificationDetails(
        presentAlert: true,  // Present an alert when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
        presentBadge: true,  // Present the badge number when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
        presentSound: true,  // Play a sound when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
    );
    const NotificationDetails platformChannelSpecifics = NotificationDetails(iOS: iOSPlatformChannelSpecifics);
    return platformChannelSpecifics;
  }

  /// Schedules notification for today
  Future<DateTime?> scheduleNotificationForToday(TimeOfDay time) async {
    DateTime now = DateTime.now();
    DateTime scheduledForHour = DateTime(now.year, now.month, now.day, time.hour, time.minute);
    return await scheduleNotification(scheduledForHour);
  }

  static int _idCounter = 0;
  Future<DateTime> scheduleNotification(DateTime scheduledForHour) async {
    var tzScheduledForHour = tz.TZDateTime.from(scheduledForHour, tz.local);
    NotificationDetails platformChannelSpecifics = _getiOSNotificationDetails();
    await _flutterLocalNotificationsPlugin.zonedSchedule(
        _idCounter++,
        'dont you dare eat now',
        'no eating after 7PM',
        tzScheduledForHour,
        platformChannelSpecifics,
        uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
        androidAllowWhileIdle: true
    );
    print('Scheduled notification for ${scheduledForHour.toString()} + id: $_idCounter');
    return scheduledForHour;
  }

  Future cancelAll() async {
    _idCounter = 0;
    await _flutterLocalNotificationsPlugin.cancelAll();
  }

}