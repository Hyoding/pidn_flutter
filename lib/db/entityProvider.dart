import 'package:pidn_mobile/constant.dart';
import 'package:pidn_mobile/model/entity.dart';
import 'package:pidn_mobile/provider/provider.dart';
import 'package:sqflite/sqflite.dart';

abstract class EntityProvider<T extends Entity> with Provider {
  late Database db;

  @override
  Future init() async {
    db = await openDatabase(Constant.dbPath, version: 1);
  }

  /// provide sql required for creating table
  String getCreateSql();

  String tableName();

  Future<T> insert(T entity) async {
    entity.id = await db.insert(tableName(), entity.toMap());
    return entity;
  }

  Future<int> update(T entity) async {
    return await db.update(tableName(), entity.toMap(),
        where: '${Entity.columnId} = ?',
        whereArgs: [entity.id]
    );
  }

  Future<int> delete(T entity) async {
    return await db.delete(tableName(),
        where: '${Entity.columnId} = ?',
        whereArgs: [entity.id]
    );
  }

}