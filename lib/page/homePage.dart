import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pidn_mobile/component/util.dart';
import 'package:pidn_mobile/model/foodItem.dart';
import 'package:pidn_mobile/model/weight.dart';
import 'package:pidn_mobile/model/win.dart';
import 'package:pidn_mobile/notification/notificationScheduler.dart';
import 'package:pidn_mobile/page/dataPage.dart';
import 'package:pidn_mobile/page/settingPage.dart';
import 'package:pidn_mobile/provider/configProvider.dart';

final DateFormat dateFormat = DateFormat("h:mm a");

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  final String title = 'PUT IT DOWN NOW';

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with WidgetsBindingObserver {
  int winStreak = 0;
  late num weightLastDay = 0;
  late num weightToday = 0.0;
  List<FoodItem> foodItems = [];

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch(state){
      case AppLifecycleState.resumed:
        await _init();
        setState(() {});
        break;
    }
  }

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  void _iAteAgain(FoodItem foodItem) async {
    await FoodItemProvider().insert(foodItem);
    setState(() {});
  }

  void _logWeight(Weight weight) async {
    await WeightProvider().logTodayWeight(weight);
    setState(() {});
  }

  _getFoodItemWidgetList() {
    return foodItems.map((foodItem) => Column(
      children: [
        const SizedBox(height: 5),
        FoodItemWidget(
          foodItem: foodItem,
          updateFoodItem: (foodItem) async {
            await FoodItemProvider().update(foodItem);
            setState(() {});
          },
          deleteFoodItem: (foodItem) async {
            await FoodItemProvider().delete(foodItem);
            setState(() {});
          },
        ),
        const SizedBox(height: 5),
      ],
    )).toList();
  }

  Future _initHomeState() async {
    // food items
    foodItems = await FoodItemProvider().getTodayList();

    // weight last day
    Weight? lastWeight = await WeightProvider().getLastDay();
    if (lastWeight != null) {
      weightLastDay = lastWeight.weight;
    }
    Weight? todayWeight = await WeightProvider().getToday();
    if (todayWeight != null) {
      weightToday = todayWeight.weight;
    }

    // log win
    Win? yesterdayWin = await WinProvider().getLastDay();
    if (yesterdayWin == null) {
      // get yesterday food items
      List<FoodItem> yesterdayFoodList = await FoodItemProvider().getYesterdayList();
      bool didWin = true;
      TimeOfDay stopEatingTime = await ConfigProvider().getStopEatingTime();
      for (var foodItem in yesterdayFoodList) {
        TimeOfDay lastEatenTime = TimeOfDay.fromDateTime(foodItem.timeStamp);
        if ((lastEatenTime.hour > stopEatingTime.hour) ||
            (lastEatenTime.hour == stopEatingTime.hour && lastEatenTime.minute > stopEatingTime.minute)
        ) { // after 6PM
          didWin =  false;
          break;
        }
      }
      Win win = Win(yesterdayFoodList.isEmpty ? false : didWin);
      await WinProvider().logYesterday(win);
    }

    winStreak = await WinProvider().getWinStreak();
  }

  Future<bool> _init() async {
    await NotificationScheduler().schedule();
    await _initHomeState();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    const TextStyle textStyleSize20 = TextStyle(
      fontSize: 20,
    );
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
              title: const Text('Fat Data', style: textStyleSize20),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const DataPage()),
                );
              },
            ),
            ListTile(
              title: const Text('Setting', style: textStyleSize20),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const SettingPage()),
                );
              },
            ),
          ],
        ),
      ),
      body: FutureBuilder<void>(
        future: _init(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            bool fatterToday = weightToday - weightLastDay > 0.0;
            return Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'win streak: $winStreak',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      OutlinedButton(
                        onPressed: () {
                          Util.showModal(
                              context: context,
                              title: 'did ur scale break?',
                              hintText: 'Enter ur fat weight',
                              textInputType: TextInputType.number,
                              cb: (userInput) {
                                Weight weight = Weight(weight: num.parse(userInput));
                                _logWeight(weight);
                              }
                          );
                        },
                        child: weightToday == 0 ? const Text('WEIGH UR FATASS', style: TextStyle(fontSize: 16)) : Row(
                          children: [
                            Text(
                              'weight: $weightToday',
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Icon(fatterToday ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                                color: fatterToday ? Colors.red : Colors.green
                            ),
                            Text('${(weightToday - weightLastDay).abs()}',
                              style: TextStyle(
                                  color: fatterToday ? Colors.red : Colors.green
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 300,
                  child: Image.network('https://www.pngkey.com/png/full/79-799682_transparent-person-obese-fat-person-cartoon-transparent.png'),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: ListView(
                      children: _getFoodItemWidgetList(),
                    ),
                  ),
                ),
                OutlinedButton(
                  style: ElevatedButton.styleFrom(
                    minimumSize: const Size.fromHeight(50),
                  ),
                  onPressed: () async {
                    final TimeOfDay? newTime = await showTimePicker(
                      helpText: 'when did u eat again?',
                      context: context,
                      initialTime: TimeOfDay.now(),
                    );
                    if (newTime == null) {
                      return;
                    }

                    DateTime timeStamp = DateTime.now();
                    timeStamp = DateTime(timeStamp.year, timeStamp.month, timeStamp.day, newTime.hour, newTime.minute);
                    Util.showModal(
                        context: context,
                        title: 'glutted at ${dateFormat.format(timeStamp)}',
                        hintText: 'Enter what u just ate',
                        cb: (userInput) {
                          FoodItem foodItem = FoodItem(
                              memo: userInput,
                              timeStamp:timeStamp,
                          );
                          _iAteAgain(foodItem);
                        }
                    );
                  },
                  child: const Text('I ATE AGAIN', style: textStyleSize20),
                ),
              ],
            );
          } else {
            return Container(
              alignment: AlignmentDirectional.center,
              child: const CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}

class FoodItemWidget extends StatelessWidget {
  static double fontSize = 20;

  final FoodItem foodItem;
  final Function(FoodItem) updateFoodItem;
  final Function(FoodItem) deleteFoodItem;
  const FoodItemWidget({
    Key? key,
    required this.foodItem,
    required this.updateFoodItem,
    required this.deleteFoodItem,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 100,
          child: Text(dateFormat.format(foodItem.timeStamp),
            style: TextStyle(
                fontSize: fontSize
            ),
          ),
        ),
        Expanded(
            child: Text(foodItem.memo,
              style: TextStyle(
                  fontSize: fontSize
              ),
            )
        ),
        IconButton(
          icon: const Icon(Icons.edit, color: Colors.red),
          onPressed: () async {
            final TimeOfDay? newTime = await showTimePicker(
              helpText: 'when did u eat again?',
              context: context,
              initialTime: TimeOfDay.fromDateTime(foodItem.timeStamp),
            );
            if (newTime == null) {
              return;
            }

            DateTime timeStamp = DateTime.now();
            timeStamp = DateTime(timeStamp.year, timeStamp.month, timeStamp.day, newTime.hour, newTime.minute);
            Util.showModal(
                context: context,
                title: 'glutted at ${dateFormat.format(timeStamp)}',
                initialValue: foodItem.memo,
                cb: (userInput) {
                  foodItem.timeStamp = timeStamp;
                  foodItem.memo = userInput;
                  updateFoodItem(foodItem);
                }
            );
          },
        ),
        IconButton(
          // splashRadius: 15,
          icon: const Icon(Icons.delete, color: Colors.red),
          onPressed:() {
            Util.showBottomSheet(
                context: context,
                title: 'are you sure?',
                cb: () {
                  deleteFoodItem(foodItem);
                  Util.showFatToast();
                },
            );
          },
        ),
      ],
    );
  }
}