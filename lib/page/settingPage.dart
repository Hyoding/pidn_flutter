import 'package:flutter/material.dart';
import 'package:pidn_mobile/component/util.dart';
import 'package:pidn_mobile/notification/notificationScheduler.dart';
import 'package:pidn_mobile/provider/configProvider.dart';
import 'package:settings_ui/settings_ui.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

const String setTimeTargetCommand = 'setTimeTargetCommand';
const String setTimeSpamStartCommand = 'setTimeSpamStartCommand';

class _SettingPageState extends State<SettingPage> {
  late TimeOfDay _timeTarget;
  late TimeOfDay _timeSpamStart;
  late double _targetWeight;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Setting'),
      ),
      body: FutureBuilder(
        future: _init(),
        builder: (_, snapshot) {
          if (snapshot.hasData) {
            return SettingsList(
              sections: [
                SettingsSection(
                  title: const Text('stop eating target time\nwin a point if no more food eaten past this time'),
                  tiles: <SettingsTile>[
                    SettingsTile.navigation(
                      leading: const Icon(Icons.alarm),
                      title: const Text('time'),
                      value: Text(_timeTarget.format(context)),
                      onPressed: (context) async {
                        await _selectTime(context, setTimeTargetCommand);
                      },
                    ),
                  ],
                ),
                SettingsSection(
                  title: const Text('spam me to stop eating'),
                  tiles: <SettingsTile>[
                    SettingsTile.navigation(
                      leading: const Icon(Icons.play_arrow),
                      title: const Text('start'),
                      value: Text(_timeSpamStart.format(context)),
                      onPressed: (context) async {
                        await _selectTime(context, setTimeSpamStartCommand);
                      },
                    ),
                    // SettingsTile.navigation(
                    //   leading: Icon(Icons.stop),
                    //   title: Text('end'),
                    //   value: Text(_time.format(context)),
                    //   onPressed: (context) async {
                    //     await _selectTime(context);
                    //   },
                    // ),
                  ],
                ),
                SettingsSection(
                  title: const Text('target weight'),
                  tiles: <SettingsTile>[
                    SettingsTile.navigation(
                      leading: const Icon(Icons.scale),
                      title: const Text('weight'),
                      value: Text(_targetWeight.toInt().toString()),
                      onPressed: (context) async {
                        Util.showModal(
                          title: 'target weight',
                          initialValue: _targetWeight.toInt().toString(),
                          textInputType: TextInputType.number,
                          context: context,
                          shouldShowFatToast: false,
                          cb: (userInput) async {
                            await ConfigProvider().setTargetWeight(double.parse(userInput));
                            setState(() {});
                          },
                        );
                      },
                    ),
                  ],
                ),
              ],
            );
          } else {
            return Container(
              alignment: AlignmentDirectional.center,
              child: const CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Future<void> _selectTime(BuildContext context, String command) async {
    TimeOfDay initialTime;
    if (command == setTimeTargetCommand) {
      initialTime = _timeTarget;
    } else {
      initialTime = _timeSpamStart;
    }
    final TimeOfDay? newTime = await showTimePicker(
      context: context,
      initialTime: initialTime,
    );
    if (newTime != null) {
      if (command == setTimeTargetCommand) {
        await ConfigProvider().setStopEatingTime(newTime);
      } else {
        await ConfigProvider().setSpamStartTime(newTime);
        await NotificationScheduler().schedule();
      }
      setState(() {});
    }
  }

  Future _init() async {
    _timeTarget = await ConfigProvider().getStopEatingTime();
    _timeSpamStart = await ConfigProvider().getSpamStartTime();
    _targetWeight = await ConfigProvider().getTargetWeight();
    return true;
  }
}
