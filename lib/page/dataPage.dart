import 'package:flutter/material.dart';
import 'package:pidn_mobile/model/weight.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:pidn_mobile/provider/configProvider.dart';

class DataPage extends StatefulWidget {
  const DataPage({Key? key}) : super(key: key);

  @override
  _DataPageState createState() => _DataPageState();
}

class _DataPageState extends State<DataPage> {
  late List<Weight> _weightList;
  late double _targetWeight;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Fat Data'),
      ),
      body: FutureBuilder(
        future: _init(),
        builder: (_, snapshot) {
          if (snapshot.hasData) {
            return Container(
              padding: const EdgeInsets.all(15),
              child: Column(
                children: [
                  Expanded(
                    flex: 1,
                    child: WeightChart(
                      weightList: _weightList,
                      targetWeight: _targetWeight,
                    ),
                  ),
                  // Expanded(
                  //   flex: 1,
                  //   child: WeightChart(
                  //     weightList: _weightList,
                  //     targetWeight: _targetWeight,
                  //   ),
                  // ),
                ],
              ),
            );
          } else {
            return Container(
              alignment: AlignmentDirectional.center,
              child: const CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Future _init() async {
    _weightList = await WeightProvider().getAll();
    _targetWeight = await ConfigProvider().getTargetWeight();
    return true;
  }
}

class WeightChart extends StatelessWidget {
  final List<Weight> weightList;
  final double targetWeight;

  const WeightChart({Key? key,
    required this.weightList,
    required this.targetWeight,
  }) : super(key: key);

  List<charts.Series<Weight, DateTime>> _chartData() {
    return [
      charts.Series<Weight, DateTime>(
        id: 'my weight',
        colorFn: (weight, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (weight, _) => DateTime.parse(weight.date),
        measureFn: (weight, _) => weight.weight,
        data: weightList,
      ),
      charts.Series<Weight, DateTime>(
        id: 'target: ${targetWeight.toInt()}',
        colorFn: (weight, __) => charts.MaterialPalette.red.shadeDefault,
        domainFn: (weight, _) => DateTime.parse(weight.date),
        measureFn: (weight, _) => targetWeight,
        data: weightList,
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    List<num> weightsOrdered = _weightListOrdered();
    final num lowestWeight = weightsOrdered.first;
    final num highestWeight = weightsOrdered.last;
    final num diff = weightList.last.weight - targetWeight;
    return Column(
      children: [
        Text('${diff.toInt().toString()} away from target'),
        Expanded(
          child: charts.TimeSeriesChart(_chartData(),
            primaryMeasureAxis: charts.NumericAxisSpec(
              // showAxisLine: true,
              // tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 1),
              viewport: charts.NumericExtents(lowestWeight - 5, highestWeight + 5),
            ),
            behaviors: [
              charts.SeriesLegend(position: charts.BehaviorPosition.bottom),
            ],
            // animate: true,
            defaultRenderer: charts.LineRendererConfig(includePoints: true),
          ),
        ),
      ],
    );
  }

  List<num> _weightListOrdered() {
    List<num> weights = [targetWeight];
    weights.addAll(weightList.map((weight) => weight.weight));
    weights.sort();
    return weights;
  }
}
