import 'package:pidn_mobile/db/entityProvider.dart';
import 'package:pidn_mobile/model/entity.dart';

const String tableFoodItem = 'foodItem';
const String columnTimeStamp = 'timeStamp';
const String columnMemo = 'memo';
const String columnCalorie = 'calorie';

class FoodItem extends Entity {
  late DateTime timeStamp;
  late String memo;
  int? calorie;

  FoodItem({required this.memo, required this.timeStamp, this.calorie});

  @override
  Map<String, Object?> toMap() {
    var map = super.toMap();
    map[columnTimeStamp] = timeStamp.toIso8601String();
    map[columnMemo] = memo;
    map[columnCalorie] = calorie;
    return map;
  }

  FoodItem.fromMap(Map<String, dynamic> map) {
    id = map[Entity.columnId];
    date = map[Entity.columnDate];
    timeStamp = DateTime.parse(map[columnTimeStamp].toString());
    memo = map[columnMemo];
    // calorie = int.parse(map[columnCalorie].toString());
  }
}

class FoodItemProvider extends EntityProvider {
  static final FoodItemProvider _provider = FoodItemProvider._internal();

  factory FoodItemProvider() {
    return _provider;
  }

  FoodItemProvider._internal();

  @override
  String getCreateSql() {
    return '''
            create table $tableFoodItem (
              ${Entity.columnId} integer primary key autoincrement,
              ${Entity.columnDate} text not null,
              $columnTimeStamp text not null,
              $columnMemo text not null,
              $columnCalorie num)
            ''';
  }

  Future<List<FoodItem>> getTodayList() async {
    String today = dateFormat.format(DateTime.now());
    return _getOneDayList(today);
  }

  Future<List<FoodItem>> getYesterdayList() async {
    String yesterday = dateFormat.format(DateTime.now().subtract(const Duration(days: 1)));
    return _getOneDayList(yesterday);
  }

  Future<List<FoodItem>> _getOneDayList(String dateStr) async {
    List<FoodItem> foodItems = [];
    List<Map<String, Object?>> records = await db.query(tableFoodItem,
        where: '${Entity.columnDate} = ? order by timeStamp',
        whereArgs: [dateStr]
    );
    for (var record in records) {
      FoodItem foodItem = FoodItem.fromMap(record);
      foodItems.add(foodItem);
    }
    return foodItems;
  }

  @override
  String tableName() {
    return tableFoodItem;
  }
}