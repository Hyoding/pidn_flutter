import 'package:intl/intl.dart';
import 'package:pidn_mobile/constant.dart';

final DateFormat dateFormat = DateFormat(Constant.dbDateFormat);

abstract class Entity {
  static String columnId = '_id';
  static String columnDate = 'date';

  int id = -1;
  String date = dateFormat.format(DateTime.now());

  Map<String, Object?> toMap() {
    var map = <String, Object?> {
      columnDate: date,
    };
    if (id != -1) {
      map[columnId] = id;
    }
    return map;
  }

}