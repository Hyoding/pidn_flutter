import 'package:pidn_mobile/db/entityProvider.dart';
import 'package:pidn_mobile/model/entity.dart';

const String tableWin = 'win';
const String columnWon = 'won';

class Win extends Entity {
  late bool won;

  Win(this.won) {
    // this is for yesterday's date
    date = dateFormat.format(DateTime.now().subtract(const Duration(days: 1)));
  }

  @override
  Map<String, Object?> toMap() {
    var map = super.toMap();
    map[columnWon] = won ? 1 : 0;
    return map;
  }

  Win.fromMap(Map<String, dynamic> map) {
    id = map[Entity.columnId];
    date = map[Entity.columnDate];
    won = map[columnWon] == 1 ? true : false;
  }
}

class WinProvider extends EntityProvider {
  static final WinProvider _provider = WinProvider._internal();

  factory WinProvider() {
    return _provider;
  }

  WinProvider._internal();

  @override
  String getCreateSql() {
    return '''
            create table $tableWin (
              ${Entity.columnId} integer primary key autoincrement,
              ${Entity.columnDate} text not null unique,
              $columnWon num not null)
            ''';
  }

  Future<Win?> getLastDay() async {
    // get yesterday
    String yesterday = dateFormat.format(DateTime.now().subtract(const Duration(days: 1)));
    return _getForDay(yesterday);
  }

  Future<Win?> _getForDay(String dateStr) async {
    // get yesterday
    List<Map<String, Object?>> records = await db.query(tableWin,
        where: '${Entity.columnDate} = ?',
        whereArgs: [dateStr]
    );
    return records.isEmpty ? null : Win.fromMap(records.first);
  }

  Future<Win> logYesterday(Win win) async {
    win.id = await db.insert(tableWin, win.toMap());
    return win;
  }

  Future<int> getWinStreak() async {
    DateTime yesterday = DateTime.now().subtract(const Duration(days: 1));
    return await _consecutiveWinsFrom(yesterday);
  }

  /// gets consecutive wins from initial date
  Future<int> _consecutiveWinsFrom(DateTime date) async {
    String dateStr = dateFormat.format(date);
    Win? win = await _getForDay(dateStr);
    if (win == null || !win.won) {
      return 0;
    }
    DateTime dayBefore = date.subtract(const Duration(days: 1));
    return 1 + await _consecutiveWinsFrom(dayBefore);
  }

  @override
  String tableName() {
    return tableWin;
  }
}