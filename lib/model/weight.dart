import 'package:pidn_mobile/db/entityProvider.dart';
import 'package:pidn_mobile/model/entity.dart';

const String tableWeight = 'weight';
const String columnWeight = 'weight';

class Weight extends Entity {
  late num weight;

  Weight({required this.weight});

  @override
  Map<String, Object?> toMap() {
    var map = super.toMap();
    map[columnWeight] = weight;
    return map;
  }

  Weight.fromMap(Map<String, dynamic> map) {
    id = map[Entity.columnId];
    date = map[Entity.columnDate];
    weight = map[columnWeight];
  }
}

class WeightProvider extends EntityProvider {
  static final WeightProvider _provider = WeightProvider._internal();

  factory WeightProvider() {
    return _provider;
  }

  WeightProvider._internal();
  
  @override
  String getCreateSql() {
    return '''
            create table $tableWeight (
              ${Entity.columnId} integer primary key autoincrement,
              ${Entity.columnDate} text not null unique,
              $columnWeight num not null)
            ''';
  }

  Future<List<Weight>> getAll() async {
    List<Weight> weightList = [];
    List<Map<String, Object?>> records = await db.query(tableWeight);
    for (var record in records) {
      Weight weight = Weight.fromMap(record);
      weightList.add(weight);
    }
    return weightList;
  }

  Future<Weight?> getLastDay() async {
    // latest id that's not today's
    String today = dateFormat.format(DateTime.now());
    List<Map<String, Object?>> records = await db.query(tableWeight,
      where: '${Entity.columnDate} != ? order by ${Entity.columnId} desc limit 1',
      whereArgs: [today]
    );
    return records.isEmpty ? null : Weight.fromMap(records.first);
  }

  Future<Weight?> getToday() async {
    String today = dateFormat.format(DateTime.now());
    var records = await db.query(tableWeight,
        where: '${Entity.columnDate} = ?',
        whereArgs: [today]
    );
    return records.isEmpty ? null : Weight.fromMap(records.first);
  }

  Future<Weight> logTodayWeight(Weight weight) async {
    // if already logged for today, then update
    Weight? weightToUpdate = await getToday();
    if (weightToUpdate == null) {
      weight.id = await db.insert(tableWeight, weight.toMap());
      return weight;
    } else {
      weightToUpdate.weight = weight.weight;
      await db.update(tableWeight, weightToUpdate.toMap(),
        where: '${Entity.columnId} = ?',
        whereArgs: [weightToUpdate.id]
      );
      return weightToUpdate;
    }
  }

  @override
  String tableName() {
    return tableWeight;
  }
}