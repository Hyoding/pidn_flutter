import 'package:pidn_mobile/constant.dart';
import 'package:pidn_mobile/db/entityProvider.dart';
import 'package:pidn_mobile/model/foodItem.dart';
import 'package:pidn_mobile/model/weight.dart';
import 'package:pidn_mobile/model/win.dart';
import 'package:pidn_mobile/provider/configProvider.dart';
import 'package:pidn_mobile/provider/provider.dart';
import 'package:sqflite/sqflite.dart';

final List<Provider> providers = [
  FoodItemProvider(),
  WeightProvider(),
  WinProvider(),
  ConfigProvider(),
];

class ProviderInitializer {
  static final ProviderInitializer _helper = ProviderInitializer._internal();

  factory ProviderInitializer() {
    return _helper;
  }

  ProviderInitializer._internal();

  Future init() async {

    // await deleteDatabase(Constant.dbPath);

    await openDatabase(Constant.dbPath, version: 1,
      onCreate: (db, version) async {
        for (var provider in providers) {
          if (provider is EntityProvider) {
            await db.execute(provider.getCreateSql());
          }
        }
    });
    for (var provider in providers) {
      await provider.init();
    }
  }
}