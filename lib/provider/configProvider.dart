import 'package:flutter/material.dart';
import 'package:pidn_mobile/provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

final TimeOfDayHelper timeHelper = TimeOfDayHelper();
const TimeOfDay defaultTime = TimeOfDay(hour: 19, minute: 0);

class ConfigProvider with Provider {
  static final ConfigProvider _provider = ConfigProvider._internal();

  factory ConfigProvider() {
    return _provider;
  }

  ConfigProvider._internal();

  late SharedPreferences _prefs;

  final String _stopEatingTimeKey = 'stopEatingTime';
  final String _spamStartTimeKey = 'spamStartTime';
  final String _targetWeightKey = 'targetWeight';

  Future<void> setStopEatingTime(TimeOfDay time) async {
    String timeStr = timeHelper.convertToString(time);
    await _prefs.setString(_stopEatingTimeKey, timeStr);
  }

  Future<TimeOfDay> getStopEatingTime() async {
    String? timeStr = _prefs.getString(_stopEatingTimeKey);
    if (timeStr == null) {
      await setStopEatingTime(defaultTime);
      return defaultTime;
    }
    return timeHelper.parse(timeStr);
  }

  Future<void> setSpamStartTime(TimeOfDay time) async {
    String timeStr = timeHelper.convertToString(time);
    await _prefs.setString(_spamStartTimeKey, timeStr);
  }

  Future<TimeOfDay> getSpamStartTime() async {
    String? timeStr = _prefs.getString(_spamStartTimeKey);
    if (timeStr == null) {
      await setSpamStartTime(defaultTime);
      return defaultTime;
    }
    return timeHelper.parse(timeStr);
  }

  Future<void> setTargetWeight(double weight) async {
    await _prefs.setDouble(_targetWeightKey, weight);
  }

  Future<double> getTargetWeight() async {
    double? targetWeight = _prefs.getDouble(_targetWeightKey);
    if (targetWeight == null) {
      await setTargetWeight(0);
      return 0;
    }
    return targetWeight;
  }

  @override
  Future init() async {
    _prefs = await SharedPreferences.getInstance();
  }
}

class TimeOfDayHelper {
  String convertToString(TimeOfDay time) {
    return '${time.hour}_${time.minute}';
  }
  TimeOfDay parse(String timeStr) {
    List<int> hrMin = timeStr.split("_").map((e) => int.parse(e)).toList();
    return TimeOfDay(hour: hrMin[0], minute: hrMin[1]);
  }
}