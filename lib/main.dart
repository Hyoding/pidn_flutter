import 'package:flutter/material.dart';
import 'package:pidn_mobile/provider/providerInitializer.dart';
import 'package:pidn_mobile/notification/notificationService.dart';
import 'package:pidn_mobile/page/homePage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await ProviderInitializer().init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PUT IT DOWN NOW',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const HomePage(),
    );
  }
}
