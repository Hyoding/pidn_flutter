import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Util {
  static void showModal({
    required BuildContext context,
    String title = '',
    String hintText = '',
    String initialValue = '',
    required Function(String) cb,
    TextInputType textInputType = TextInputType.text,
    bool shouldShowFatToast = true,
  }) {
    showDialog(
      context: context,
      builder: (context) {
        TextEditingController textController = TextEditingController(text: initialValue);
        bool canSave = initialValue.isNotEmpty;
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              title: Text(title),
              content: SizedBox(
                child: TextField(
                  onChanged: (str) {
                    setState(() {
                      canSave = str.isNotEmpty;
                    });
                  },
                  autofocus: true,
                  keyboardType: textInputType,
                  controller: textController,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: hintText,
                  ),
                ),
              ),
              actionsAlignment: MainAxisAlignment.center,
              actions: [
                ElevatedButton(
                  child: const Text('save',
                    style: TextStyle(fontSize: 20),
                  ),
                  onPressed: canSave ? () {
                    Navigator.pop(context);
                    cb(textController.text);
                    if (shouldShowFatToast) {
                      showFatToast();
                    }
                  } : null,
                )
              ],
            );
          },
        );
      },
    );
  }

  static void showFatToast() {
    Fluttertoast.showToast(
        msg: _showFatMessage(),
        timeInSecForIosWeb: 2,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 20.0
    );
  }

  static void showBottomSheet({
    required BuildContext context,
    String title = '',
    Function? cb,
  }) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 200,
          color: Colors.amber,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              // mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(title, textScaleFactor: 1.2),
                ElevatedButton(
                  child: const Text('ok', textScaleFactor: 1.2),
                  onPressed: () {
                    if (cb != null) {
                      cb();
                    }
                    Navigator.pop(context);
                  },
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

String _showFatMessage() {
  var fatMessageList = [
    "too fat", "eww", "nasty", "seriously?", "what's wrong with you", "u r joking right?",
    "gross", "stop eating", "what do u think u r doing?", "do u really wanna do this?",
    "u r gonna feel like crap after", "food coma for hrs", "dont be crazy"
  ];
  return (fatMessageList..shuffle()).first;
}